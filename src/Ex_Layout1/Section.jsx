import React, { Component } from "react";
import SectionItem from "./SectionItem";
export default class Section extends Component {
  render() {
    return (
      <div>
        <section className="pt-4">
          <div className="container px-lg-5">
            {/* Page Features*/}
            <div className="row gx-lg-5">
              <SectionItem />
              <SectionItem />
              <SectionItem />
              <SectionItem />
              <SectionItem />
              <SectionItem />
            </div>
          </div>
        </section>
      </div>
    );
  }
}
