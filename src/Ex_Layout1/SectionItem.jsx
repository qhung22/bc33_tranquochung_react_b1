import React, { Component } from "react";

export default class SectionItem extends Component {
  render() {
    return (
      <div className="col-lg-4 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div
              className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4"
              style={{
                display: "inline-block",
                alignItems: "center",
                justifyContent: "center",
                height: "4rem",
                width: "4rem",
                fontSize: "2rem",
                position: "relative",
                top: "-60px",
              }}
            >
              <i
                class="fab fa-500px"
                style={{
                  position: "relative",
                  textAlign: "center",
                  top: "7px",
                }}
              ></i>
            </div>
            <h2
              className="fs-4 fw-bold"
              style={{
                textAlign: "center",
              }}
            >
              Fresh new layout
            </h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
      </div>
    );
  }
}
