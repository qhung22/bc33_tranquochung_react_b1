import React, { Component } from "react";
import Footer from "./Footer";
import Header from "./Header";
import Nav from "./Nav";
import Section from "./Section";

export default class LayOut extends Component {
  render() {
    return (
      <div>
        <Nav />
        <Header />
        <Section />
        <Footer />
      </div>
    );
  }
}
